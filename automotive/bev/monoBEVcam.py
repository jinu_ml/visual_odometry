import sys
import cv2
import numpy as np

def read_cam():
    cap = cv2.VideoCapture(4)

    if cap.isOpened():
        cv2.namedWindow("demo", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo1", cv2.WINDOW_AUTOSIZE)


        while True:
            ret_val, img = cap.read();
    
            #pts1 = np.float32([[535,550],[714,550],[0,700],[1280,720]])
            #pts2 = np.float32([[0,0],[500,0],[0,500],[500,500]])

           #pts1 = np.float32([[535,550],[714,550],[0,700],[1280,720]])
           #pts2 = np.float32([[0,0],[500,0],[0,500],[500,500]])
            pts1 = np.float32([[336,491],[952,495],[1271,711],[2,715]])
            pts2 = np.float32([[0,0],[1024,0],[1024,720],[0,720]])


            M = cv2.getPerspectiveTransform(pts1,pts2)
            print(M)
            dst = cv2.warpPerspective(img,M,(1280,720), flags=cv2.INTER_LINEAR)


            imgres= np.array(cv2.resize(img,(500,500)))
            imgres1= np.array(cv2.resize(dst,(500,500)))

            numpy_horizontal1 = np.concatenate((imgres, imgres1),axis=1)

            cv2.imshow("birdview", numpy_horizontal1)
            cv2.waitKey(10)


    else:
     print("camera open failed")

    cv2.destroyAllWindows()


if __name__ == '__main__':
    read_cam()
