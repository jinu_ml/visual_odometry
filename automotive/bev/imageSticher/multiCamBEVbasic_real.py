import numpy as np
import cv2


def birdeye():

    cap = cv2.VideoCapture('v4l2src device=\"/dev/video0\" ! videoconvert ! appsink')
    cap1 = cv2.VideoCapture('v4l2src device=\"/dev/video1\" ! videoconvert ! appsink')
    cap2 = cv2.VideoCapture('v4l2src device=\"/dev/video2\" ! videoconvert ! appsink')
    cap3 = cv2.VideoCapture('v4l2src device=\"/dev/video3\" ! videoconvert ! appsink')
    cap4 = cv2.VideoCapture('v4l2src device=\"/dev/video4\" ! videoconvert ! appsink')
    cap5 = cv2.VideoCapture('v4l2src device=\"/dev/video5\" ! videoconvert ! appsink')
    cap6 = cv2.VideoCapture('v4l2src device=\"/dev/video6\" ! videoconvert ! appsink')
    cap7 = cv2.VideoCapture('v4l2src device=\"/dev/video7\" ! videoconvert ! appsink')

    
    print('_'*5)
    if cap.isOpened():
        cv2.namedWindow("demo0", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo1", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo2", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo3", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo4", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo5", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo6", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo7", cv2.WINDOW_AUTOSIZE)
        while True:

            ret0, frame0 = cap.read()
            ret1, frame1 = cap1.read()
            ret2, frame2 = cap2.read()

            ret3, frame3 = cap3.read()
            ret4, frame4 = cap4.read()
            ret5, frame5 = cap5.read()

    
            ret6, frame6 = cap6.read()
            ret7, frame7 = cap7.read()

            # Checking if video run or finished


            # Adjust your 4 points
#             pts1 = np.float32([[535,550],[714,550],[0,700],[1280,720]])
#             pts2 = np.float32([[0,0],[500,0],[0,500],[500,500]])

            pts1 = np.float32([[839,568],[1138,574],[1164,621],[806,612]])
            pts2 = np.float32([[317,34],[893,47],[881,478],[272,439]])

            M = cv2.getPerspectiveTransform(pts1,pts2)
#             dst = cv2.warpPerspective(frame,M,(500,500))
        
            dst0 = cv2.warpPerspective(frame0,M,(1280,1280))
            dst1 = cv2.warpPerspective(frame1,M,(1280,1280))
            dst2 = cv2.warpPerspective(frame2,M,(1280,1280))
            dst3 = cv2.warpPerspective(frame3,M,(1280,1280))
            dst4 = cv2.warpPerspective(frame4,M,(1280,1280))
            dst5 = cv2.warpPerspective(frame5,M,(1280,1280))
            dst6 = cv2.warpPerspective(frame6,M,(1280,1280))
            dst7 = cv2.warpPerspective(frame7,M,(1280,1280))
        
            cam_main= np.array(cv2.resize(dst1,(426,240)))
            cam_narrow= np.array(cv2.resize(dst0,(426,240)))
            cam_wide = np.array(cv2.resize(dst2,(426,240)))
            
            cam_orvm_right= np.array(cv2.resize(dst3,(426,240)))
            cam_bpillar_right= np.array(cv2.resize(dst4,(426,240)))

            cam_orvm_left= np.array(cv2.resize(dst5,(426,240)))            
            cam_bpillar_left= np.array(cv2.resize(dst6,(426,240)))
            cam_back= np.array(cv2.resize(dst7,(426,240)))


            numpy_horizontal1 = np.concatenate((cam_narrow,cam_main,cam_wide),axis=1)
            numpy_horizontal2 = np.concatenate((cam_bpillar_left,cam_back,cam_bpillar_right),axis=1)
            birdview = np.concatenate( (numpy_horizontal1,numpy_horizontal2), axis=0)


            cv2.imshow("birdview", birdview)
            cv2.waitKey(10)

        
    #         # Exit by pressing 'q'
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break
    else:
        print('camera open failed')
    cv2.destroyAllWindows()
    cap.release()

if __name__== '__main__':

    birdeye()
