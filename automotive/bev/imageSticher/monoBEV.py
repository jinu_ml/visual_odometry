import numpy as np
import cv2


def birdeye():

#    cap = cv2.VideoCapture('car_dashcam.mp4')  # This is an online link for my video
    #cap = cv2.VideoCapture('road.mp4')     # Write your video name
#     cap = cv2.VideoCapture('../../motion_planning/data/driving_downtown.mp4')
    cap = cv2.VideoCapture('CAM_BACK.mp4') 

    if cap.isOpened():
        cv2.namedWindow("demo", cv2.WINDOW_AUTOSIZE)

        while True:

            ret, frame = cap.read()
            # Checking if video run or finished


            # Adjust your 4 points
#             pts1 = np.float32([[535,550],[714,550],[0,700],[1280,720]])
#             pts2 = np.float32([[0,0],[500,0],[0,500],[500,500]])

            pts1 = np.float32([[839,568],[1138,574],[1164,621],[806,612]])
            pts2 = np.float32([[317,34],[893,47],[881,478],[272,439]])

            M = cv2.getPerspectiveTransform(pts1,pts2)
#             dst = cv2.warpPerspective(frame,M,(500,500))
        
            dst = cv2.warpPerspective(frame,M,(720,1280))
        
            imgres= np.array(cv2.resize(frame,(426,240)))
            imgres1= np.array(cv2.resize(dst,(426,240)))
            
#            cv2.imshow('Input',imgres)
#            cv2.imshow('Output',imgres1)

            numpy_horizontal1 = np.concatenate((imgres, imgres1),axis=1)
            cv2.imshow("birdview", numpy_horizontal1)
            cv2.waitKey(10)

        
    #         # Exit by pressing 'q'
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break
    else:
        print('camera open failed')
    cv2.destroyAllWindows()
    cap.release()
        
if __name__== '__main__':

    birdeye()
