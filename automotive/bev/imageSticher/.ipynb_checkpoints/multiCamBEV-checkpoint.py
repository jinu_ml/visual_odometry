import numpy as np
import cv2


def birdeye():

#    cap = cv2.VideoCapture('car_dashcam.mp4')  # This is an online link for my video
    #cap = cv2.VideoCapture('road.mp4')     # Write your video name
#     cap = cv2.VideoCapture('../../motion_planning/data/driving_downtown.mp4')
    cap = cv2.VideoCapture('CAM_FRONT.mp4') 
    cap1 = cv2.VideoCapture('CAM_FRONT_LEFT.mp4') 
#     cap2 = cv2.VideoCapture('CAM_FRONT_RIGHT.mp4') 
    print('_'*5)
    if cap.isOpened():
        cv2.namedWindow("demo0", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo1", cv2.WINDOW_AUTOSIZE)
#         cv2.namedWindow("demo2", cv2.WINDOW_AUTOSIZE)
        while True:

            ret0, frame0 = cap.read()
            ret1, frame1 = cap1.read()
#             ret1, frame2 = cap2.read()
            # Checking if video run or finished


            # Adjust your 4 points
#             pts1 = np.float32([[535,550],[714,550],[0,700],[1280,720]])
#             pts2 = np.float32([[0,0],[500,0],[0,500],[500,500]])

            pts1 = np.float32([[839,568],[1138,574],[1164,621],[806,612]])
            pts2 = np.float32([[317,34],[893,47],[881,478],[272,439]])

            M = cv2.getPerspectiveTransform(pts1,pts2)
#             dst = cv2.warpPerspective(frame,M,(500,500))
        
            dst0 = cv2.warpPerspective(frame0,M,(2480,1600))
            dst1 = cv2.warpPerspective(frame1,M,(2480,1600))
#             dst2 = cv2.warpPerspective(frame2,M,(720,1280))
        
            cam_front= np.array(cv2.resize(dst0,(426,240)))
            cam_front_left= np.array(cv2.resize(dst1,(426,240)))
#             cam_front_right= np.array(cv2.resize(dst2,(426,240)))
            
            
            img1 = cv2.cvtColor(dst0,cv2.COLOR_BGR2GRAY)
            img2 = cv2.cvtColor(dst1,cv2.COLOR_BGR2GRAY)
            
            sift = cv2.SIFT_create()
            kp1, des1 = sift.detectAndCompute(img1,None)
            kp2, des2 = sift.detectAndCompute(img2,None)
            
            bf = cv2.BFMatcher()
            matches = bf.knnMatch(des1,des2, k=2)
            
            good = []
            for m in matches:
                if (m[0].distance < 0.5*m[1].distance):
                    good.append(m)
                    
            matches = np.asarray(good)
            print('*'*5)
            print(f'matches array -> :{matches}')
            
            assert matches is not None
            
#             if (len(matches[:,0]) >= 4):
            if (len(matches) >= 4):
                src = np.float32([ kp1[m.queryIdx].pt for m in matches[:,0] ]).reshape(-1,1,2)
                dst = np.float32([ kp2[m.trainIdx].pt for m in matches[:,0] ]).reshape(-1,1,2)
                H, masked = cv2.findHomography(src, dst, cv2.RANSAC, 5.0)
            else:
#                 raise AssertionError('Can’t find enough keypoints.')
                numpy_horizontal1 = np.concatenate((cam_front_left,cam_front),axis=1)
                cv2.imshow("birdview", numpy_horizontal1)
                continue
            
        
            dst = cv2.warpPerspective(frame0,H,((frame0.shape[1] + frame1.shape[1]), frame1.shape[0])) #wraped image
            dst[0:frame1.shape[0], 0:frame1.shape[1]] = frame1 #stitched image

            cv2.imshow('stichedBird',frame1)

            
#             numpy_horizontal1 = np.concatenate((cam_front_left,cam_front,cam_front_right),axis=1)
#             cv2.imshow("birdview", numpy_horizontal1)
            cv2.waitKey(10)

        
    #         # Exit by pressing 'q'
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break
    else:
        print('camera open failed')
    cv2.destroyAllWindows()
    cap.release()

if __name__== '__main__':

    birdeye()
