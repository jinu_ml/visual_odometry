#!/bin/sh

gst-launch-1.0 -e  v4l2src  do-timestamp=true device=/dev/video$1 ! nvvidconv ! 'video/x-raw, format=UYVY, width=1280, height=720, framerate=30/1' ! qtmux ! filesink location=data/cam$1/recorded_`date '+%Y-%m-%d_%H-%M-%S'`.mp4

