import gi 
import sys
from time import sleep

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject

Gst.init(sys.argv)

pipeline = Gst.parse_launch('v4l2src do-timestamp=true device=/dev/video0 ! image/jpeg,width=1920,height=1080,framerate=30/1 ! nvv4l2decoder mjpeg=1 ! nvvidconv flip-method=2 ! clockoverlay halignment=left valignment=bottom text="Dashcam" shaded-background=true font-desc="Sans, 12" ! omxh264enc ! "video/x-h264, streamformat=(string)byte-stream" ! h264parse ! ! qtmux ! filesink location=video.mp4')

def main():
  bus = pipeline.get_bus()      
  pipeline.set_state(Gst.State.PLAYING)
  print('Recording Dashcam')
  
  sleep(10) 
  print('Sending EOS')
  pipeline.send_event(Gst.Event.new_eos())
  print('Waiting for EOS')
  bus.timed_pop_filtered(Gst.CLOCK_TIME_NONE, Gst.MessageType.EOS)
  pipeline.set_state(Gst.State.NULL)

main()
