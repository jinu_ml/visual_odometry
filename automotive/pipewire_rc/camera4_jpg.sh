#!/bin/sh

cd ~/Desktop/01_AR0147-Camera-Driver/

#chmod +x ./init_ar0231
echo nvidia | sudo -S ./init_ar0147_1205_J46.sh

echo "EQ Set"
sudo i2cset -f -y 2 0x48 0x32 0x99
sudo i2cset -f -y 2 0x48 0x33 0x99
sudo i2cset -f -y 7 0x48 0x32 0x99
sudo i2cset -f -y 7 0x48 0x33 0x99

cd /home/nvidia/dashcam/pipewire_rc/

echo "camera rec . . . "

gst-launch-1.0 -e \
	v4l2src device=/dev/video0 ! nvvidconv ! 'video/x-raw, format=UYVY, width=1280, height=720, framerate=30/1' ! queue ! nvvidconv ! jpegenc ! multifilesink location=data/cam0/recorded_`date '+%Y-%m-%d_%H-%M-%S'`"frame%d.jpeg"\
	v4l2src device=/dev/video1 ! nvvidconv ! 'video/x-raw, format=UYVY, width=1280, height=720, framerate=30/1' ! queue ! nvvidconv ! jpegenc ! multifilesink location=data/cam1/recorded_`date '+%Y-%m-%d_%H-%M-%S'`"frame%d.jpeg"\
	v4l2src device=/dev/video2 ! nvvidconv ! 'video/x-raw, format=UYVY, width=1280, height=720, framerate=30/1' ! queue ! nvvidconv ! jpegenc ! multifilesink location=data/cam2/recorded_`date '+%Y-%m-%d_%H-%M-%S'`"frame%d.jpeg"\
	v4l2src device=/dev/video3 ! nvvidconv ! 'video/x-raw, format=UYVY, width=1280, height=720, framerate=30/1' ! queue ! nvvidconv ! jpegenc ! multifilesink location=data/cam3/recorded_`date '+%Y-%m-%d_%H-%M-%S'`"frame%d.jpeg"\



