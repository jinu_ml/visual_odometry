import sys
import cv2

def read_cam():
    cap = cv2.VideoCapture('v4l2src device=\"/dev/video0\" ! videoconvert ! appsink')
    cap1 = cv2.VideoCapture('v4l2src device=\"/dev/video1\" ! videoconvert ! appsink')
    if cap.isOpened() and cap1.isOpened():
        cv2.namedWindow("demo", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo1", cv2.WINDOW_AUTOSIZE)
        while True:
            ret_val, img = cap.read();
            ret_1, img1 = cap1.read();
            cv2.imshow('demo',img)
            cv2.imshow('demo1',img1)
            cv2.waitKey(10)
    else:
     print("camera open failed")

    cv2.destroyAllWindows()


if __name__ == '__main__':
    read_cam()
