import sys
import cv2
import numpy as np

def read_cam():
    cap = cv2.VideoCapture(0)
    cap1 = cv2.VideoCapture(1)
    cap2 = cv2.VideoCapture(2)
    cap3 = cv2.VideoCapture(3)
    cap4 = cv2.VideoCapture(4)
    cap5 = cv2.VideoCapture(5)
    cap6 = cv2.VideoCapture(6)
    cap7 = cv2.VideoCapture(7)
    if cap.isOpened() and cap1.isOpened():
        cv2.namedWindow("demo", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo1", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo2", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo3", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo4", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo5", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo6", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("demo7", cv2.WINDOW_AUTOSIZE)

        while True:
            ret_val, img = cap.read();
            ret_1, img1 = cap1.read();
            ret_2, img2 = cap2.read();
            ret_3, img3 = cap3.read();
            ret_4, img4 = cap4.read();
            ret_5, img5 = cap5.read();
            ret_6, img6 = cap6.read();
            ret_7, img7 = cap7.read();
            
            imgres= cv2.resize(img,(426,240))
            imgres1=cv2.resize(img1,(426,240))
            imgres2=cv2.resize(img2,(426,240))
            imgres3=cv2.resize(img3,(426,240))
            imgres4=cv2.resize(img4,(426,240))
            imgres5=cv2.resize(img5,(426,240))
            imgres6=cv2.resize(img6,(426,240))
            imgres7=cv2.resize(img7,(426,240))

            numpy_horizontal1 = np.concatenate((imgres, imgres1),axis=1)
            numpy_horizontal2 = np.concatenate((imgres2, imgres3),axis=1)
            numpy_horizontal3 = np.concatenate((imgres4, imgres5),axis=1)
            numpy_horizontal4 = np.concatenate((imgres6, imgres7),axis=1)

            vidbuffer = np.concatenate( (numpy_horizontal1,numpy_horizontal2,numpy_horizontal3,numpy_horizontal4), axis=0)

            cv2.imshow("arenae", vidbuffer)
            cv2.waitKey(10)
    else:
     print("camera open failed")

    cv2.destroyAllWindows()


if __name__ == '__main__':
    read_cam()
