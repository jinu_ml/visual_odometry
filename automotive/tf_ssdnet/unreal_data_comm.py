# coding: utf-8
# # Object Detection Demo
# License: Apache License 2.0 (https://github.com/tensorflow/models/blob/master/LICENSE)
# source: https://github.com/tensorflow/models
import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
# from grabscreen import grab_screen
import cv2

from mss import mss

import socket
import sys
import json
import json5
from json import JSONEncoder

##to grab the screen
sct = mss()

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")


# ## Object detection imports
# Here are the imports from the object detection module.

from utils import label_map_util
from utils import visualization_utils as vis_util


# # Model preparation 
# What model to download.
MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90
'''
# ## Download Model
opener = urllib.request.URLopener()
opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
tar_file = tarfile.open(MODEL_FILE)
for file in tar_file.getmembers():
  file_name = os.path.basename(file.name)
  if 'frozen_inference_graph.pb' in file_name:
    tar_file.extract(file, os.getcwd())
'''
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# HOST, PORT = "localhost", 5050
sock.connect((socket.gethostbyname(socket.gethostname()), 5050))


# sock.bind((HOST, PORT))
# sock.connect((HOST, PORT))

# ## Load a (frozen) Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.compat.v1.GraphDef()
  with tf.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


# ## Loading label map
# Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

class NumpyArrayEncoder(JSONEncoder):
  def default(self, obj):
    if isinstance(obj, np.ndarray):
          return obj.tolist()
    return JSONEncoder.default(self, obj)


# ## Helper code
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


# function to add to JSON
def write_json(new_data, filename='road_data2.json'):
    with open(filename,'r+') as file:
          # First we load existing data into a dict.
        file_data = json.load(file)
        # Join new_data with file_data inside emp_details
        file_data["object_detection"].append(new_data)
        # Sets file's current position at offset.
        file.seek(0)
        # convert back to json.
        json.dump(file_data, file, indent = 4)

# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)
data_z={}
with detection_graph.as_default():
  with tf.compat.v1.Session(graph=detection_graph) as sess:
    while True:
      w, h = 800, 450
      monitor = {'top': 50, 'left': 100, 'width': w, 'height': h}
      img = Image.frombytes('RGB', (w,h), sct.grab(monitor).rgb)   
      screen = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
      #screen = cv2.resize(grab_screen(region=(0,40,1280,745)), (WIDTH,HEIGHT))
      # screen = cv2.resize(grab_screen(region=(0,40,1280,745)), (800,450))
      image_np = cv2.cvtColor(screen, cv2.COLOR_BGR2RGB)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
          
      # Visualization of the results of a detection.
      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=2)


      im_width = 24
      im_height = 14
      # print(boxes.shape) 
      ##rescale the boxes  
      def scaled_boxes(boxes,im_width,im_height):
        bbox_scaled = np.empty_like(boxes)
        for i,b in enumerate(boxes[0]):
            ls = [] 
            if classes[0][i] == 3 or classes[0][i] == 6 or classes[0][i] == 8:
              if scores[0][i] >= 0.5:
                x = boxes[0][i][0]* im_width
                y = boxes[0][i][1]* im_height
                xw= boxes[0][i][2]* im_width
                yh= boxes[0][i][3]* im_height
                rescaled = np.array([x,y,xw,yh])
                ls.append(rescaled)
                ls =np.array(ls)
                bbox_scaled[0, i] = ls
        return bbox_scaled

      '''Detected classes labels'''
      def filtered_classes(classes):
        cs=[]
        for i,b in enumerate(boxes[0]): 

          # #               car                    bus                  truck
          if classes[0][i] == 3 or classes[0][i] == 6 or classes[0][i] == 8:
            if scores[0][i] >= 0.5:
              # print(50*'$')
              # print(classes[0][i])
              # np.append(cs,classes[0][i])
              cs.append(classes[0][i])
              # print(cs)
        return np.array(cs).reshape(-1,1)
 
      '''this part of the code give central cordinates'''

      def boxes_centroid(boxes,im_width,im_height,classes):
        ls = []    
        for i,b in enumerate(boxes[0]):

            if classes[0][i] == 3 or classes[0][i] == 6 or classes[0][i] == 8:
              if scores[0][i] >= 0.5:
                ymin = boxes[0][i][0]* im_height
                xmin = boxes[0][i][1]* im_width
                ymax= boxes[0][i][2]* im_height
                xmax= boxes[0][i][3]* im_width
                x_mid= (xmax-xmin)/2
                y_mid =(ymax-ymin)/2
                rescaled = np.append(x_mid,y_mid)
                ls.append(rescaled)
        return np.array(ls).reshape(-1,2)




      # ''' divides the region as extreme_left, left, center, right, extreme_right '''
      # def detect_region(boxes,im_width,classes):
        
      #   region = np.empty([1,100,1]) 
      #   for i,b in enumerate(boxes[0]):
      #     lane_region = [] 
      #     if classes[0][i] == 3 or classes[0][i] == 6 or classes[0][i] == 8:
      #       if scores[0][i] >= 0.5:
      #         object_marker = boxes[0][i][0]*im_width
      #         marker= im_width/5
      #         if object_marker<=marker:
      #           lane_region.append(int(1))
      #         elif object_marker>marker and object_marker<=2*marker:
      #           lane_region.append(int(2))
      #         elif object_marker>2*marker and object_marker<=3*marker:
      #           lane_region.append(int(3))
      #         elif object_marker>3*marker and object_marker<=4*marker:
      #           lane_region.append(int(4))
      #         else:
      #           lane_region.append(int(5))
      #         lane_region= np.array(lane_region)
      #         region[0,i]=lane_region
      #   return region



      def distance_estimate(boxes,classes):
        ds=[]
        for i,b in enumerate(boxes[0]): 
          # #               car                    bus                  truck
          if classes[0][i] == 3 or classes[0][i] == 6 or classes[0][i] == 8:
            if scores[0][i] >= 0.5:
              apx_distance = round(((1 - (boxes[0][i][3] - boxes[0][i][1]))**4),1)
              np_dist = np.array([apx_distance])
              ds.append(np_dist)
              # ds= np.array(ds)
        return np.array(ds).reshape(-1,1)
            
      #payload being sent yo unreal through socket 
      unreal_payload = {"object_detection":{"Label_id":filtered_classes(classes), "bboxes_centroid":boxes_centroid(boxes,im_width,im_height,classes), "distance":distance_estimate(boxes,classes),}}
      # unreal_payload = {'Label_id':classes, 'bboxes':boxes, 'region': 'center' }
      print(unreal_payload)

      # data_z+=unreal_payload
      # print(boxes_centroid(boxes,im_width,im_height,classes))
      # print(distance_estimate(boxes,classes))
    #   print(scaled_boxes(boxes,im_width,im_height))
      # jsonObj = json5.dumps(unreal_payload,cls=NumpyArrayEncoder,indent=3 ,separators=(',', ': '))
                #  Writing to sample.json
      jsonObj = json.dumps(unreal_payload,cls=NumpyArrayEncoder)

      # write_json(jsonObj)

      # with open("road_data1.json", "a") as outfile:
      #     json.dump(jsonObj,outfile,indent=4, sort_keys=True,
      #                 separators=('\\', ': '), ensure_ascii=False)

      with open("road_data1.json", "w") as outfile:
          print(jsonObj,file=outfile,end="\n")



      ##TCP socket communication

      sock.sendall(bytes(jsonObj, encoding="utf-8"))

            # Receive data from the server and shut down
      # received = sock.recv(1024)
      # received = received.decode("utf-8")
      # print(received)
      
    
      for i,b in enumerate(boxes[0]):
        #                 car                    bus                  truck
        if classes[0][i] == 3 or classes[0][i] == 6 or classes[0][i] == 8:
          if scores[0][i] >= 0.5:
            mid_x = (boxes[0][i][1]+boxes[0][i][3])/2
            mid_y = (boxes[0][i][0]+boxes[0][i][2])/2
            apx_distance = round(((1 - (boxes[0][i][3] - boxes[0][i][1]))**4),1)
            # apx_distance = round(((1 - (boxes[0][i][2] - boxes[0][i][0]))**4),1)
            cv2.putText(image_np, '{}'.format(apx_distance), (int(mid_x*800),int(mid_y*450)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,255,255), 2)

            if apx_distance <=0.5:
              if mid_x > 0.3 and mid_x < 0.7:
                cv2.putText(image_np, 'WARNING!!!', (50,50), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 3)


      cv2.imshow('window',cv2.resize(image_np,(800,450)))
      if cv2.waitKey(25) & 0xFF == ord('q'):
          cv2.destroyAllWindows()
          sock.close()
          break
